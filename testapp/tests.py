from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.apps import apps
from datetime import datetime
from django.utils import timezone
from .views import index
from .apps import *
from .models import UserInput
from .forms import status_input
import time

# Create your tests here.
class Testapp_UnitTest(TestCase):

    def test_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_index_contains_greeting(self):
        response = Client().get('')
        response_content = response.content.decode('utf-8')
        self.assertIn("Hello World", response_content)

    def test_apps(self):
        self.assertEqual(TestappConfig.name, 'testapp')
        self.assertEqual(apps.get_app_config('testapp').name, 'testapp')

    def test_model_can_create_new_status(self):
        status_input = UserInput.objects.create(status_user='Status')

        counting_all_status_input = UserInput.objects.all().count()
        self.assertEqual(counting_all_status_input, 1)
    
    def test_form_validation_for_blank_field(self):
        form = status_input(data={'status_user': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status_user'],
            ["This field is required."]
        )
    
    def test_form_validation_for_filled_items(self) :
        response = self.client.post('', data={'status_user' : 'Status'})
        response_content = response.content.decode()
        self.assertIn(response_content, 'Status')

    def test_form_name(self) :
        response = self.client.post('', data={'status_user' : 'Status'})
        status = UserInput.objects.get(pk=1)
        self.assertEqual(str(status), status.status_user)
    
    def test_home_post_success_and_render_the_result(self):
        test = ''
        response_post = Client().post('', {'status_name': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_home_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('', {'status_name': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_valid_input(self):
        status_form = status_input({'status_user': "Status"})
        self.assertTrue(status_form.is_valid())
        status = UserInput()
        status.status_user = status_form.cleaned_data['status_user']
        status.save()
        self.assertEqual(status.status_user, "Status")

    def test_invalid_input(self):
        status_form = status_input({
            'status_name': 301 * "X"
        })
        self.assertFalse(status_form.is_valid())
    
from django import forms
from .models import UserInput

class status_input(forms.Form):
    status_user = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : '',
        'type' : 'text',
        'maxlength' : '',
        'required' : True,
        'style' : '',
        'label' : '',
    }))